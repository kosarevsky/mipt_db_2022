# МФТИ 2022, весна. Базы данных. Семинары. 

![](seminars/imgs/img.png)

## [Семинар № 1. Реляционная алгебра](seminars/seminar_01.md)[^1]
## [Семинар № 2. SQL-запросы](seminars/seminar_02.md)[^2]
## [Семинар № 3. SQL-запросы. Соединения](seminars/seminar_03.md)[^3]
## [Семинар № 4. TCL. Проектирование БД](seminars/seminar_04.md)[^4]
## [Семинар № 5. Проектирование. Ограничения на поля. Подзапросы.](seminars/seminar_05.md)[^5]
## [Семинар № 6. Оконные функции.](seminars/seminar_06.md)[^6]
## [Семинар № 7. Общие табличные выражения (CTE). Представления (Views).](seminars/seminar_07.md)[^7]
## [Семинар № 8. Хранимый код. Функции и триггеры.](seminars/seminar_08.md)[^8]
## [Семинар № 9. Работа с базами данных в Python. Курсоры. ORM.](seminars/seminar_09.ipynb)[^9]
## [Семинар № 10. EXPLAIN & ANALYZE. Индексы.](seminars/seminar_10.md)[^10]
## [Семинар № 11. ETL.](seminars/seminar_11.md)[^11]
## [Семинар № 12. Погружение в Индексы.](seminars/seminar_12.md)[^12]

---

## [Домашние задания](https://gitlab.com/fpmi-atp/db2022-supplementary/global/-/blob/master/homeworks/README.md) на sql-ex.ru[^20]

---

[^1]: [Калькулятор реляционной алгебры](https://dbis-uibk.github.io/relax/calc/local/uibk/local/0)
[^2]: [Postgres и pgAdmin. Запуск из Docker контейнера](docker/readme_docker.md)
[^3]: [SQLize.online is a free online SQL environment for quickly running, experimenting with and sharing SQL code.](https://sqlize.online/sql/psql14/9cffb8e3d397e93627eb41cd55b10c20/)
[^4]: [DataGrip](https://www.jetbrains.com/ru-ru/datagrip/)
[^5]: [Выражения подзапросов](https://postgrespro.ru/docs/postgresql/14/functions-subquery)
[^6]: [Тест знаний по оконным функциям](https://www.windowfunctions.com/)
[^7]: [Запросы `WITH` (Общие табличные выражения)](https://postgrespro.ru/docs/postgresql/14/queries-with)
[^8]: [Создание функций](https://postgrespro.ru/docs/postgresql/14/sql-createfunction)
[^9]: [PEP 249](https://peps.python.org/pep-0249/)
[^10]: [Использование EXPLAIN](https://postgrespro.ru/docs/postgresql/14/using-explain)
[^11]: [ETL](https://ru.wikipedia.org/wiki/ETL)
[^12]: [Индексы](https://postgrespro.ru/docs/postgresql/14/indexes-intro)
[^20]: [sql-ex.ru](https://www.sql-ex.ru/exercises/index.php?act=learn&LN=1/)
