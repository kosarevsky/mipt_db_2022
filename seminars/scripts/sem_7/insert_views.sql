create schema if not exists seminar_7;

create table if not exists seminar_7.organization as
select 1 as id_org, 'АО Тинькофф Банк' as name_org
union
select 2, 'ВТБ'
union
select 3, 'Сбер';

create table if not exists seminar_7.teacher (id_teach, last_name, first_name, birth_date, salary_amt, id_org) as
select 1, 'Роздухова', 'Нина', '1992-04-15', 15000.00, 1
union
select 2, 'Меркурьева', 'Надежда', '1995-03-12', 25000.00, 1
union
select 3, 'Халяпов', 'Александр', '1994-09-30', 17000.00, 2
union
select 4, 'Иванов', 'Иван', NULL, 100000.00, 3
union
select 5, 'Петров', 'Петр', NULL, 3000.00, 3;
