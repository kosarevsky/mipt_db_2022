create schema if not exists seminar_7;

create table if not exists seminar_7.department (dep_id, par_dep_id, dep_name) as
select 1, NULL, 'Банк' union
select 2, 1, 'Управление анализа кредитных рисков' union
select 3, 2, 'Отдел риск-менеджмента малого и среднего бизнеса' union
select 4, 2, 'Отдел риск-менеджмента розничного бизнеса' union
select 5, 1, 'Департамент ИТ' union
select 6, 5, 'Управление хранилищ данных и отчетности' union
select 7, 6, 'Отдел очистки и контроля качества данных' union
select 8, 7, 'Группа администрирования хранилищ данных' union
select 9, 7, 'Группа контроля качества данных' union
select 10, 5, 'Отдел отчетности и витрин данных' union
select 11, 5, 'Отдел трансформации и загрузки данных' union
select 12, 11, 'Группа системного анализа' union
select 13, 11, 'Группа разработки';
