# [Базы данных.](https://gitlab.com/kosarevsky/mipt_db_2022/-/blob/main/README.md)

## Семинар 12. Погружение в Индексы[^1]

![](imgs/sem_12/img.png)

### 1. Интро

Самый больной вопрос для любого разработчика, которому приходится вычитывать данные из базы: "Как сделать мой запрос
быстрее?". Классический ответ - необходимо создать подходящий индекс. Но куда именно его стоит применять, да и как
вообще он должен выглядеть?

Сегодня подробно рассмотрим некоторые рекомендации, которые помогут нам понять когда правильный индекс действительно
может помочь улучшить производительность запроса.

И "правильный" - тут ключевое слово, потому что создать ряд неправильных индексов, которые нормально работать все равно
не будут - наука невелика.

**Индексы** — объекты базы данных, предназначенные в основном для ускорения доступа к данным. Это вспомогательные структуры:
любой индекс можно удалить и восстановить заново по информации в таблице. Но, кроме ускорения, индексы служат и для
поддержки некоторых ограничений целостности.

В ядро встроены шесть индексных методов доступа (типов индексов), увидеть их можно выполнив запрос:

```postgresql
SELECT amname FROM pg_am WHERE amtype = 'i';
```

Для удобства можно использовать вспомогательную функцию, для очистки индексов на таблице:

```postgresql
CREATE OR REPLACE FUNCTION drop_table_indexes(sch text, tbl text) RETURNS INTEGER AS
$$
DECLARE
    i RECORD;
BEGIN
    FOR i IN
        (SELECT * FROM pg_indexes
         WHERE tablename = tbl AND schemaname = sch)
        LOOP
            EXECUTE 'DROP INDEX ' || i.schemaname || '.' || i.indexname;
        END LOOP;
    RETURN 1;
END;
$$ LANGUAGE plpgsql;
```

Функция принимает два параметра, имя схемы и имя таблицы.

Проверить какие индексы есть на той или иной таблице можно следующим запросом:

```postgresql
select * from pg_indexes where tablename like '%pg_class_copy%';
```

#### Проблема #1: Seq Scan

Создадим таблицу из системной таблицы `pg_class`, и будем проводить некоторые эксперименты над ее копией:

```postgresql
DROP SCHEMA IF EXISTS seminar_12 CASCADE;
CREATE SCHEMA IF NOT EXISTS seminar_12;
CREATE TABLE IF NOT EXISTS seminar_12.pg_class_copy AS TABLE pg_class;
```

Посмотрим, как реагирует база на попытку найти идентификаторы всех последовательностей (sequence), которые есть у нас в базе:

```postgresql
EXPLAIN (ANALYZE, BUFFERS, COSTS OFF, VERBOSE)
SELECT oid FROM seminar_12.pg_class_copy 
WHERE relkind = 'S';
```

Поскольку ни одного индекса на таблице нет, то получим самый простой вариант, который может встретиться в плане `Seq Scan`:

```postgresql
Seq Scan on seminar_12.pg_class_copy (actual time=0.111..0.123 rows=0 loops=1)
  Output: oid
  Filter: (pg_class_copy.relkind = 'S'::""char"")
  Rows Removed by Filter: 400
  Buffers: shared hit=11
Planning:
  Buffers: shared hit=13
Planning Time: 0.179 ms
Execution Time: 0.197 ms
```

`Filter` - это как раз то самое условие, которое заставило `PostgreSQL` из 400 прочитанных записей отбросить 400 и не оставить
ни одной. И это плохо - прочитали в 400 раз больше необходимого количества записей!

#### Способы индексации

**Индекс по полю/выражению**

Очевидно, что сразу напрашивается индекс по значению поля `pg_class_copy`(`relkind`):

```postgresql
CREATE INDEX ON seminar_12.pg_class_copy USING btree(relkind);
```

Видно, что вместо фильтрации выражение теперь ушло в условие индексного поиска `Index Cond`, 
а сам узел превратился в `Index Scan`:

```postgresql
Index Scan using pg_class_copy_relkind_idx on seminar_12.pg_class_copy (actual time=0.030..0.039 rows=0 loops=1)
  Output: oid
  Index Cond: (pg_class_copy.relkind = 'S'::""char"")
  Buffers: shared read=1
Planning:
  Buffers: shared hit=19 read=1
Planning Time: 0.256 ms
Execution Time: 0.097 ms
```

Очистим индексы:

```postgresql
select drop_table_indexes('seminar_12', 'pg_class_copy');
```

**Индекс с условием**

С другой стороны, можно все выражение вынести в `WHERE`-условие индекса, а его поля использовать под что-то более
насущное - например, под тот самый `oid`, который вычитывается:

```postgresql
CREATE INDEX ON seminar_12.pg_class_copy USING btree(oid) -- индексируемое поле
WHERE relkind = 'S';                                      -- условие применения индекса
```

Важно обратить внимание на то, что узел превратился в `Index Only Scan`, а вот вынесенное нами условие исчезло из плана вовсе:

```postgresql
Index Only Scan using pg_class_copy_oid_idx on seminar_12.pg_class_copy (actual time=0.009..0.020 rows=0 loops=1)
  Output: oid
  Heap Fetches: 0
  Buffers: shared hit=1
Planning:
  Buffers: shared hit=19 read=1
Planning Time: 0.413 ms
Execution Time: 0.068 ms
```

Такое поведение характерно только для узлов `Index [Only] Scan` - в `Seq Scan` нам и так видны все условия сразу, а
в `Bitmap Heap Scan` увидим условия дополнительной проверки записей страниц в строке `Recheck Cond`.

Очистим индексы:

```postgresql
select drop_table_indexes('seminar_12', 'pg_class_copy');
```

**Неиндексируемые выражения**

Но далеко не все выражения можно проиндексировать, а только сводящиеся к `IMMUTABLE` в терминах категорий изменчивости
функций.

Применительно к индексам это означает, что выражение должно быть вычислимо с использованием только полей записи и
`IMMUTABLE`-функций, выдающих всегда один и тот же результат.

Если на совсем простых примерах:

![](imgs/sem_12/img_1.png)

На что обратить внимание:

1) Из всех этих вариантов условий, только первый допускает создание индекса (...) `WHERE ts >= '2022-04-01 00:00:00'::
   timestamp`, во всех остальных случаях правая часть не является иммутабельной.

2) Хотя все **три последних** варианта математически эквивалентны, однако:

- первый может использовать наиболее общий индекс и не зависит от константы
- второй требует специального индекса с фиксацией константы
- третий не может быть проиндексирован из-за вариативности `now()` и является примером одной из классических ошибок при
использовании индексов

#### Типы индексов

Пока мы использовали только `btree` - самый простой и "дефолтный" из всех типов индексов[^2], которые `PostgreSQL`
поддерживает "из коробки", его явное указание можно опускать при описании индекса:

```postgresql
CREATE INDEX ON seminar_12.pg_class_copy(relkind);
```

Но типов индексов[^2] в `PostgreSQL` гораздо больше, и каждый из них обладает своими возможностями и ограничениями, но
основное отличие - поддерживаемые типы полей (на самом деле, конечно, произвольных `IMMUTABLE`-выражений от них) и
операторы.

Некоторые из них могут быть модифицированы с использованием дополнительных расширений, поэтому рассмотрим только базовые
возможности доступные "из коробки".

**btree**

Индекс btree, он же B-дерево, пригоден для данных, которые можно отсортировать. Иными словами, для типа данных должны
быть определены операторы «больше», «больше или равно», «меньше», «меньше или равно» и «равно».

Операторы линейного порядка (`<`, `<=`, `=`, `>=`, `>`):

- числовые (`smallint`, `integer`, `bigint`, `numeric`, `real`, `double precision`)
- хронологические (`date`, `time`, `time without time zone`, `time with time zone`, `timestamp`, `timestamp without time zone`, `timestamp with time zone`)
- `uuid`
- текстовые (`varchar`, `text`)

Операторы префиксного поиска (`~<~`, `~<=~`, `~`, `~>=~`, `~>~`) с использованием дополнительных классов операторов[^3]:

- текстовые (`varchar`, `text`)

Подробнее в обзоре по [ссылке](https://habr.com/ru/company/postgrespro/blog/330544/).

**hash**

Многие современные языки программирования включают хеш-таблицы в качестве базового типа данных. Внешне это выглядит, как
обычный массив, но в качестве индекса используется не целое число, а любой тип данных (например, строка). Хеш-индекс в
`PostgreSQL` устроен похожим образом.

Индекс может содержать только одно поле и поддерживает только один оператор `=`, поэтому в реальной работе малоприменим.

Подробнее в обзоре по [ссылке](https://habr.com/ru/company/postgrespro/blog/328280/).

**GiST**

GiST — сокращение от «generalized search tree». Это сбалансированное дерево поиска, точно так же, как и рассмотренный
ранее `b-tree`.[^4]

Операторы геометрических отношений[^5] (`<<`, `&<`, `&>`, `>>`, `<@`, `@>`, `~=`, `&&`, `<<|`, `&<|`, `|&>`, `|>>`, `~`, `@`):

- геометрические (`box`, `circle`, `poly`, `point`)

Операторы для сетевых адресов[^6] (`<<`, `<<=`, `>>`, `>>=`, `=`, `<>`, `<`, `<=`, `>`, `>=`, `&&`):

- сетевые адреса (`inet`)

Операторы для диапазонов[^7] (`=`, `&&`, `@>`, `<@`, `<<`, `>>`, `&<`, `&>`, `-|-`):

- диапазоны числовые (`int4range`, `int8range`, `numrange`)
- диапазоны хронологические (`daterange`, `tsrange`, `tstzrange`)

Операторы текстового поиска[^8] (`<@`, `@>`, `@@`) :

- FTS-типы (`tsquery`, `tsvector`)

Дополнительно:

поддерживает оператор упорядочивания `<->`, который позволяет осуществлять индексный kNN-поиск
при использовании расширения `btree_gist` поддерживает операторы и типы `btree`

Подробнее в обзоре по [ссылке](https://habr.com/ru/company/postgrespro/blog/333878/).

**SP-GiST**

Вначале немного о названии. Слово «GiST» намекает на определенную схожесть с одноименным методом. Схожесть действительно
есть: и тот, и другой — generalized search trees, обобщенные деревья поиска, предоставляющие каркас для построения
разных методов доступа.

«SP» расшифровывается как space partitioning, разбиение пространства. В роли пространства часто выступает именно то, что
мы и привыкли называть пространством — например, двумерная плоскость. Но, как мы увидим, имеется в виду любое
пространство поиска, по сути произвольная область значений.

SP-GiST подходит для структур, в которых пространство рекурсивно разбивается на непересекающиеся области. В этот класс
входят деревья квадрантов (`quadtree`), k-мерные деревья (`k-D tree`), префиксные деревья (`trie`).

Индекс может содержать только одно поле и поддерживает те же возможности, что и у `gist`, включая оператор
упорядочивания `<->`, но с большей оптимизацией для пространственных данных.

Подробнее в обзоре по [ссылке](https://habr.com/ru/company/postgrespro/blog/337502/).

**GIN**

GIN расшифровывается как Generalized Inverted Index — это так называемый обратный индекс. Он работает с типами данных,
значения которых не являются атомарными, а состоят из элементов. При этом индексируются не сами значения, а отдельные
элементы; каждый элемент ссылается на те значения, в которых он встречается.[^9]

Операторы для массивов[^10] (`&&`, `@>`, `<@`, `=`):

- массивы (`anyarray`)

Операторы jsonb-ключей[^11] (`@>`, `@?`, `@@`, `?`, `?|`, `?&`):

- `jsonb`

Операторы jsonb-путей[^12] (`@>`, `@?`, `@@`):

- `jsonb`

Операторы текстового поиска[^8] (`@@`, `@@@`) :

- `tsvector`

Дополнительно:

- при использовании расширения `btree_gin`[^13] поддерживает операторы и типы `btree`

Подробнее в обзоре по [ссылке](https://habr.com/ru/company/postgrespro/blog/340978/).

**BRIN**

Блочный индекс с возможностями `btree`.

В отличие от индексов, с которыми мы уже познакомились, идея BRIN не в том, чтобы быстро найти нужные строки, а в том,
чтобы избежать просмотра заведомо ненужных. Это всегда неточный индекс: он вообще не содержит TID-ов табличных строк.

Упрощенно говоря, `BRIN` хорошо работает для тех столбцов, значения в которых коррелируют с их физическим расположением в
таблице. Иными словами, если запрос без предложения ORDER BY выдает значения столбца практически в порядке возрастания
или убывания (и при этом по столбцу нет индексов).

С одной стороны, позволяет эффективно индексировать большие блоки данных, с другой - неэффективен для небольших,
поскольку всегда получается `Bitmap Heap Scan`.

Подробнее в обзоре по [ссылке](https://habr.com/ru/company/postgrespro/blog/346460/).

#### Условия применимости

Из приведенных выше особенностей следует достаточно простой алгоритм, когда и какой тип можно попробовать использовать,
а когда не стоит:

- поддержка нужного оператора;

не стоит пытаться использовать `btree`-индекс, если у вас условие с `<>`, но если у вас `~>=~`, не забудьте 
`text_pattern_ops`.

- поддержка нужного типа;

хотите что-то искать в `jsonb` - только `gin`, если надо сочетать `<`, `=`, `>` и `<@` - смотрим на `btree_gist`
/`btree_gin`.

- поддержка нескольких полей;

если необходима, то `hash` и `spgist` сразу отпадают.

- количество данных;

если возвращается мало записей, то `brin` вам не нужен.

#### Проблема #2: BitmapAnd

Соблазн использовать модель `EAV` (Entity-Attribute-Value)[^14] при организации структуры БД весьма велик, особенно когда
предметная область заранее плохо известна (или разработчик просто не хочет в нее углубляться). Это ведь так удобно -
создать "универсальный" способ описания характеристик объектов, который больше не потребует доработок базы ни при
появлении новых типов объектов, ни при возникновении новых атрибутов...

![](imgs/sem_12/img_2.png)

Для демонстрации второй проблемы создадим таблицу:

```postgresql
CREATE TABLE seminar_12.tst_eav AS
SELECT (random() * 1e4)::integer e -- 10k объектов
     , (random() * 1e2)::integer a -- 100 характеристик
     , (random() * 1e2)::integer v -- 100 вариантов значений
FROM generate_series(1, 1e6);      -- 1M записей о значениях
```

Добавим индексы:

```postgresql
CREATE INDEX ON seminar_12.tst_eav(a);
CREATE INDEX ON seminar_12.tst_eav(v);
```

Пробуем отобрать сразу по двум ключам:

```postgresql
explain (analyze, buffers, costs off)
SELECT * FROM seminar_12.tst_eav WHERE a = 1 AND v = 1;
```

Увидим следующий вывод:

```postgresql
Bitmap Heap Scan on tst_eav (actual time=4.229..5.927 rows=93 loops=1)
  Recheck Cond: ((a = 1) AND (v = 1))
  Heap Blocks: exact=93
  Buffers: shared hit=115
  ->  BitmapAnd (actual time=4.087..4.136 rows=0 loops=1)
        Buffers: shared hit=22
        ->  Bitmap Index Scan on tst_eav_a_idx (actual time=1.941..1.950 rows=10092 loops=1)
              Index Cond: (a = 1)
              Buffers: shared hit=11
        ->  Bitmap Index Scan on tst_eav_v_idx (actual time=1.726..1.737 rows=10023 loops=1)
              Index Cond: (v = 1)
              Buffers: shared hit=11
Planning Time: 0.406 ms
Execution Time: 7.016 ms
```

Очевидно, что каждый из `Bitmap Index Scan` "наметил" к дальнейшей проверке по ~10k записей, а всего нам их оказалось нужно
`93`. Посмотрим внимательно на Recheck Cond - там два условия, которые мы можем комбинировать как в варианте с Seq Scan:

```postgresql
(a, v)
(a) WHERE v = 1
(v) WHERE a = 1
(?) WHERE a = 1 AND v = 1
```

Попробуем первый вариант с составным индексом как наиболее типовой:

```postgresql
CREATE INDEX ON seminar_12.tst_eav(a, v);
```

Повторим отбор по двум ключам:

```postgresql
explain (analyze, buffers, costs off)
SELECT * FROM seminar_12.tst_eav WHERE a = 1 AND v = 1;
```

Увидим следующий вывод:

```postgresql
Bitmap Heap Scan on tst_eav (actual time=0.089..1.626 rows=93 loops=1)
  Recheck Cond: ((a = 1) AND (v = 1))
  Heap Blocks: exact=93
  Buffers: shared hit=93 read=3
  ->  Bitmap Index Scan on tst_eav_a_v_idx (actual time=0.056..0.065 rows=93 loops=1)
        Index Cond: ((a = 1) AND (v = 1))
        Buffers: shared read=3
Planning:
  Buffers: shared hit=36 read=1
Planning Time: 0.301 ms
Execution Time: 3.195 ms
```

Теперь вместо 22 страниц данных мы прочитали всего 3 - и это хорошо!

#### Проблема #3: Limit - Sort - Scan

Немного модифицируем предыдущий запрос, и найдем запись с минимальным `v` для конкретного заданного `a`:

```postgresql
explain (analyze, buffers, costs off)
SELECT * FROM seminar_12.tst_eav WHERE a = 1 ORDER BY v LIMIT 1;
```

Получим вывод:

```postgresql
Limit (actual time=116.063..118.649 rows=1 loops=1)
  Buffers: shared hit=2602 read=2918
  ->  Gather Merge (actual time=116.044..118.604 rows=1 loops=1)
        Workers Planned: 2
        Workers Launched: 2
        Buffers: shared hit=2602 read=2918
        ->  Sort (actual time=104.019..104.043 rows=1 loops=3)
              Sort Key: v
              Sort Method: top-N heapsort  Memory: 25kB
              Buffers: shared hit=2602 read=2918
              Worker 0:  Sort Method: top-N heapsort  Memory: 25kB
              Worker 1:  Sort Method: top-N heapsort  Memory: 25kB
              ->  Parallel Seq Scan on tst_eav (actual time=0.046..67.568 rows=3364 loops=3)
                    Filter: (a = 1)
                    Rows Removed by Filter: 329969
                    Buffers: shared hit=2488 read=2918
Planning:
  Buffers: shared hit=5 dirtied=1
Planning Time: 0.327 ms
Execution Time: 118.765 ms
```

"Параллельность" `Seq Scan` не должна нас смущать, и для условия a = 1 мы ровно как в первом случае можем порекомендовать
варианты индексов:

```postgresql
(a)
(?) WHERE a = 1
```

Но если мы поднимемся выше, то увидим, что Sort-узел хранит информацию о дополнительно используемых полях: Sort Key: v.
Так почему бы нам не расширить индексы ключом сортировки?

```postgresql
(a, v)
(v) WHERE a = 1
```

Попробуем первый из них (a, v) - тем более, он же рассматривался и в предыдущей проблеме:

```postgresql
CREATE INDEX ON seminar_12.tst_eav(a, v);
```

Повторим запрос:

```postgresql
explain (analyze, buffers, costs off)
SELECT * FROM seminar_12.tst_eav WHERE a = 1 ORDER BY v LIMIT 1;
```

Получим вывод:

```postgresql
Limit (actual time=0.131..0.165 rows=1 loops=1)
  Buffers: shared hit=1 read=3
  ->  Index Scan using tst_eav_a_v_idx on tst_eav (actual time=0.107..0.115 rows=1 loops=1)
        Index Cond: (a = 1)
        Buffers: shared hit=1 read=3
Planning:
  Buffers: shared hit=18 read=1
Planning Time: 0.771 ms
Execution Time: 0.243 ms
```

Видно, что наш запрос значительно ускорился! Но при дальнейшей оптимизации надо быть много аккуратнее - в плане теперь
вообще не фигурирует условие сортировки по `v` .

Важно отметить, что такое расширение индекса имеет смысл только в случае использования оператора `=` или `IS NULL` для 
всех остальных полей, иначе это не сможет использоваться эффективно[^15]. То есть, например, для условия `a > 1` - увы, 
оптимизация не даст эффекта.

#### Итоги

**Индексы** — важнейший инструмент баз данных, ускоряющий поиск.

Если индексы такие замечательные и отлично помогают СУБД в работе, то почему бы нам не построить индексы на все поля
для всех таблиц?

Тут мы подошли к темной стороне индексов. Действительно это очень полезный инструмент, но он не бесплатен, создавать
много индексов без лишней необходимости не стоит — индексы занимают дополнительную память.

Индексы занимают место и на диске и в памяти. К примеру на тестовой таблице с ~9 млн. записей хеш-индекс по полю 
может занимать 256Мб, а b-tree индекс уже 507 Мб.

Индексы замедляют запись в таблицу, так как базе данных необходимо поддерживать индексы в консистентном состоянии, а для
этого их нужно обновлять (перестраивать) в случае обновления данных.

Что ещё почитать:
Официальная документация + [цикл статей](https://habr.com/ru/company/postgrespro/blog/326096/) от разработчиков СУБД
Postgres Pro.

![](imgs/sem_12/img_3.png)

Книга [POSTGRESQL 14 ИЗНУТРИ](https://edu.postgrespro.ru/postgresql_internals-14.pdf) от тех же разработчиков,
электронная версия распространяется свободно.

---

[^1]: [Индексы](https://postgrespro.ru/docs/postgresql/14/indexes-intro)
[^2]: [Типы индексов](https://postgrespro.ru/docs/postgresql/14/indexes-types)
[^3]: [Семейства и классы операторов](https://postgrespro.ru/docs/postgresql/14/indexes-opclass)
[^4]: [Встроенные классы операторов GiST](https://postgrespro.ru/docs/postgresql/14/gist-builtin-opclasses#GIST-BUILTIN-OPCLASSES-TABLE)
[^5]: [Геометрические функции и операторы](https://postgrespro.ru/docs/postgresql/14/functions-geometry)
[^6]: [Функции и операторы для работы с сетевыми адресами](https://postgrespro.ru/docs/postgresql/14/functions-net)
[^7]: [Диапазонные/мультидиапазонные функции и операторы](https://postgrespro.ru/docs/postgresql/14/functions-range)
[^8]: [Функции и операторы текстового поиска](https://postgrespro.ru/docs/postgresql/14/functions-textsearch)
[^9]: [Встроенные классы операторов GIN](https://postgrespro.ru/docs/postgresql/14/gin-builtin-opclasses#GIN-BUILTIN-OPCLASSES-TABLE)
[^10]: [Функции и операторы для работы с массивами](https://postgrespro.ru/docs/postgresql/14/functions-array)
[^11]: [Функции и операторы JSON](https://postgrespro.ru/docs/postgresql/14/functions-json)
[^12]: [Язык путей SQL/JSON](https://postgrespro.ru/docs/postgresql/14/functions-json#FUNCTIONS-SQLJSON-PATH)
[^13]: [btree_gin](https://postgrespro.ru/docs/postgresql/14/btree-gin)
[^14]: [SQL HowTo: разные варианты работы с EAV](https://habr.com/ru/company/tensor/blog/657895/)
[^15]: [DBA: находим бесполезные индексы](https://habr.com/ru/company/tensor/blog/488104/)
